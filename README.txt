Info:
A partir dos ficheiros fornecidos, opt�mos por fazer o programa em c++. S� o 'gzip.cpp' foi alterado.

Todas as fun��es apresentam um pequeno texto em coment�rio explicando o seu objectivo. O c�digo est� organizado pela ordem de chamada de fun��es imposta pelo enunciado do trabalho.

Para visualizar a tabela de resultados esperados para os c�digos de Huffman de cada etapa do algoritmo, basta descomentar a linha "#define DEBUG" no inicio do 'gzip.cpp'.


************* Instru��es para correr o programa *************

Para Linux:

Compilar:
g++ -Wall gzip.cpp -o gzip 

Executar:
./gzip filename 
(ex: ./gzip FAQ.txt.gz) 


