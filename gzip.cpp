/*Author: Rui Pedro Paiva
Teoria da Informa��o, LEI, 2007/2008*/
/*Modificado por
Ana Rita Pascoa 2010129292
Madalena Fernandinho - 2012135350
*/

#include <cstdlib>
#include "huffman.cpp"
#include "gzip.h"

#define DEBUG

int* get_lengths(HuffmanTree *hft, int *litcomp, int lim, unsigned int *rb, char *availBits, FILE* gzFile);
void getHuffmanCode (int* ccc, int* codHuff_c,int* maxc,int lim);
void codigoHuffman2tree(HuffmanTree *hft, int *codigoHuffman, int *ccc, int maxc, int limite,int* ordem);
unsigned char* inflate(HuffmanTree *HT_LIT, HuffmanTree *HT_DIST, unsigned int *rb,char *availBits, FILE* gzFile,unsigned char* buff, int *ind);

//auxiliares
void getBits(unsigned int *rb, int needBits, char* availBits, FILE* gzFile, int *valor);
int needBits2Mask(int needBits);
void sortArray(int* ordem, int* ccc, int n);

//conversores
int bin2dec(char *bin);
void dec2bin(int decimal, char *binary);

//fun��o principal, a qual gere todo o processo de descompacta��o
int main(int argc, char** argv)
{
	//--- Gzip file management variables
	FILE *gzFile,*outputFile;  //ponteiro para o ficheiro a abrir
	long fileSize;
	long origFileSize;
	int numBlocks = 0;
	gzipHeader gzh;
	unsigned char byte;  //vari�vel tempor�ria para armazenar um byte lido directamente do ficheiro
	unsigned int rb = 0;  //�ltimo byte lido (poder� ter mais que 8 bits, se tiverem sobrado alguns de leituras anteriores)
	unsigned char* file_buffer; //construtor do ficheiro descompactado baseado em LZ77
	char needBits = 0, availBits = 0;
    int HLIT,HDIST,HCLEN;
    int valor; //recebe os bits lidos do bloco
    int ordem[]={16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};
    int *ccc, *codHuff_c; //variaveis para determinar os codigos de Huffman do alfabeto de comprimentos de codigo
    int maxc = 0; //variavel auxiliar para determinar o tamanho maximo da string (comprimento em binario) a enviar para a arore de huffman
    int *array_lit,*comp_litcomp,*codHuff_l; //variaveis para determinar os codigos de Huffman do alfabeto de literais/comprimento
    int *array_dist,*comp_dist,*codHuff_d; //variaveis para determinar os codigos de Huffman do alfabeto de distancias
    int ind=0; //ultima posi�ao lida do file_buffer

	//--- obter ficheiro a descompactar
	//char fileName[] = "FAQ.txt.gz";

	if (argc != 2)
	{
		printf("Linha de comando invalida!!!");
		return -1;
	}
	char * fileName = argv[1];

	//--- processar ficheiro
	gzFile = fopen(fileName, "r");
	fseek(gzFile, 0L, SEEK_END);
	fileSize = ftell(gzFile);
	fseek(gzFile, 0L, SEEK_SET);

	//ler tamanho do ficheiro original (acrescentar: e definir Vector com s�mbolos
	origFileSize = getOrigFileSize(gzFile);


	//--- ler cabe�alho
	int erro = getHeader(gzFile, &gzh);
	if (erro != 0)
	{
		printf ("Formato invalido!!!");
		return -1;
	}

	//--- Para todos os blocos encontrados
	char BFINAL;
	
	file_buffer = new unsigned char [origFileSize]; //cria buffer com o tamanho maximo do ficheiro original

	do
	{
		//--- ler o block header: primeiro byte depois do cabe�alho do ficheiro
		needBits = 3;
		if (availBits < needBits)
		{
			fread(&byte, 1, 1, gzFile);
			rb = (byte << availBits) | rb;
			availBits += 8;
		}

		//obter BFINAL
		//ver se � o �ltimo bloco
		BFINAL = rb & 0x01; //primeiro bit � o menos significativo
		rb = rb >> 1; //descartar o bit correspondente ao BFINAL
		availBits -=1;

		//analisar block header e ver se � huffman din�mico
		if (!isDynamicHuffman(rb))  //ignorar bloco se n�o for Huffman din�mico
			continue;
		rb = rb >> 2; //descartar os 2 bits correspondentes ao BTYPE
		availBits -= 2;

        //#################################################################################################################################
		/******************************************************************* EXERCICIO 1 *******************************************************************/
		/*le o formato do bloco (i.e., devolva o valor
        correspondente a HLIT, HDIST e HCLEN), de acordo com a estrutura de
        cada bloco*/

        //le o hlit, chama uma funcao para converter o binario para decimal
        getBits(&rb, 5, &availBits, gzFile, &valor);
        HLIT = valor;

        //le o hdist, chama a funcao para converter o binario a decimal
        getBits(&rb, 5, &availBits, gzFile, &valor);
        HDIST=valor;

        //le o hclen, chama a funcao para converter o binario a decimal
        getBits(&rb, 4, &availBits, gzFile, &valor);
        HCLEN=valor;

#ifdef DEBUG
		printf("BFINAL = %d\n", BFINAL);
		printf("HCLEN  = %d\n", HCLEN);
        printf("HLIT   = %d\n", HLIT);
        printf("HDIST  = %d\n", HDIST);

#endif
		/******************************************************************* EXERCICIO 2 *******************************************************************/
        /*armazena num array os comprimentos dos c�digos
        do alfabeto de comprimentos de c�digos, com base em HCLEN*/

        ccc = new int [HCLEN+4];

        //inicia os processos restantes
        for (int i=0;i<HCLEN+4;i++)
        {
            getBits(&rb, 3, &availBits, gzFile, &valor);
            ccc[i]=valor;
        }

		/******************************************************************* EXERCICIO 3 *******************************************************************/
        /*converte os comprimentos dos c�digos da al�nea
        anterior em c�digos de Huffman do "alfabeto de comprimentos de
        c�digos"*/
        
		sortArray(ordem,ccc,HCLEN+4);
		
		codHuff_c = new int [HCLEN+4];
        memset(codHuff_c,-1, sizeof(int)*(HCLEN+4));

		getHuffmanCode(ccc,codHuff_c,&maxc,HCLEN+4);

#ifdef DEBUG
		printf("\n\t\tComprimentos de Codigo\n");
		printf("\n\t  Comprimento\tCodigo Huffman   ->   Binario\n");
#endif

		// Inserir cada valor do codHuff_c para a �rvore HT_LEN, em bin�rio
		HuffmanTree *HT_LEN = createHFTree();
		codigoHuffman2tree(HT_LEN, codHuff_c, ccc, maxc,HCLEN+4,ordem);
		
		delete [] ccc;
		delete [] codHuff_c;

 		/******************************************************************* EXERCICIOS 4 e 6 *******************************************************************/
		/*armazena num array os HLIT + 257 comprimentos dos c�digos
		referentes ao alfabeto de literais/comprimentos, codificados
		segundo o c�digo de Huffman de comprimentos de c�digos*/

		comp_litcomp = new int [HLIT+257];
		comp_litcomp=get_lengths(HT_LEN, comp_litcomp, HLIT+257, &rb, &availBits, gzFile);

		array_lit = new int [257+HLIT];

		for (int i=0; i < 257+HLIT; i++) {
            array_lit[i] = i;
        }
		sortArray(array_lit,comp_litcomp,HLIT+257);

		maxc=0;

		codHuff_l = new int [HLIT+257];
		memset(codHuff_l,-1, sizeof(int)*(HLIT+257));

		getHuffmanCode(comp_litcomp,codHuff_l,&maxc,HLIT+257);

#ifdef DEBUG
		printf("\n\n\t\tComprimentos Literais\n");
		printf("\n\t  Comprimento\tCodigo Huffman   ->   Binario\n");
#endif

		// inserir cada valor do codHuff_l para a �rvore HT_LIT, em bin�rio
		HuffmanTree *HT_LIT = createHFTree();
		codigoHuffman2tree(HT_LIT, codHuff_l, comp_litcomp, maxc,HLIT+257,array_lit);
		
		delete [] array_lit;
		delete [] comp_litcomp;
		delete [] codHuff_l;

 		/******************************************************************* EXERCICIO 5 e 6*******************************************************************/
		/*armazena num array os HDIST + 1 comprimentos dos c�digos
		referentes ao alfabeto de distancia, codificados
		segundo o c�digo de Huffman de comprimentos de c�digos*/

		comp_dist = new int [HDIST+1];
		comp_dist=get_lengths(HT_LEN, comp_dist, HDIST+1, &rb, &availBits, gzFile);

		array_dist = new int [HDIST+1];

		for (int i=0; i < HDIST+1; i++) {
            array_dist[i] = i;
        }
		sortArray(array_dist,comp_dist,HDIST+1);

		maxc=0;

        codHuff_d = new int [HDIST+1];
		memset(codHuff_d,-1, sizeof(int)*(HDIST+1));

		getHuffmanCode(comp_dist,codHuff_d,&maxc,HDIST+1);

#ifdef DEBUG
		printf("\n\n\t\tComprimentos de Distancia a Recuar\n");
		printf("\n\t  Comprimento\tCodigo Huffman   ->   Binario\n");
#endif

		// inserir cada valor do codHuff_d para a �rvore HT_DIST, em bin�rio
		HuffmanTree *HT_DIST = createHFTree();
		codigoHuffman2tree(HT_DIST, codHuff_d, comp_dist, maxc,HDIST+1,array_dist);
		
		delete [] array_dist;
		delete [] comp_dist;
		delete [] codHuff_d;
		
		/******************************************************************* EXERCICIO 7 *******************************************************************/
		/*armazena num array os dados decompactados recorrendo 
		 �s arvores de Huffman e ao algoritmo LZ77*/
		 
		file_buffer=inflate(HT_LIT, HT_DIST, &rb, &availBits, gzFile,file_buffer, &ind);

		//actualizar n�mero de blocos analisados
		numBlocks++;
	}while(BFINAL == 0);
	
		/******************************************************************* EXERCICIO 8 *******************************************************************/
	/*grava os dados descompactados num ficheiro com o nome original*/

	if ((outputFile=fopen((gzh).fName, "w"))){
#ifdef DEBUG
		printf("\n\n\t************** FICHEIRO DESCOMPACTADO **************\n");
#endif
		for (int i=0;i<origFileSize;i++){
		#ifdef DEBUG
			printf("%c",file_buffer[i]);
		#endif
			fprintf(outputFile,"%c",file_buffer[i]);
		}
		fclose(outputFile);
	}
	else
		printf("\nErro ao criar o ficheiro %s \n",(gzh).fName);	
	
	//termina��es
	delete [] file_buffer;
	fclose(gzFile);
	printf("End: %d bloco(s) analisado(s).\n", numBlocks);

/*
    //teste da fun��o bits2String: RETIRAR antes de criar o execut�vel final
	char str[9];
	bits2String(str, 0x03);
	printf("%s\n", str);


    //RETIRAR antes de criar o execut�vel final
	system("PAUSE");*/
    return EXIT_SUCCESS;
}

void getHuffmanCode (int* ccc, int* codHuff_c,int *maxc, int lim){
        int* bl_count = new int [lim];
        int minc,w=0;

        // determinar o bl_count (numero de ocorrencias)
        for (int i=0;i<lim;i++)
            for (int j=0;j<lim;j++)
                if(ccc[j]==i)
                    bl_count[i]=bl_count[i]+1;

        // Determinar o numero maximo de bits
        for (int i = 0; i < lim; i++)
            if (*maxc< ccc[i])
                *maxc = ccc[i];

        // Determinar o numero minimo de bits
        minc = *maxc;
        for (int i = 0; i < lim; i++)
            if (minc > ccc[i] && ccc[i] > 0)
                minc = ccc[i];

        // Determinar codigos de Huffman do "alfabeto de comprimentos de codigos"
        for (int i = minc; i <= *maxc; i++, w *= 2)
            for (int j = 0; j < lim; j++)
                if (i == ccc[j])
                    codHuff_c[j] = w++;
                    
		delete [] bl_count;
}

void codigoHuffman2tree(HuffmanTree *hft, int *codigoHuffman, int *ccc, int maxcomp, int limite,int *ordem) {
	int  i, j, aux;
	char *str = new char [maxcomp+1];
	char *straux = new char [maxcomp+1];

	for (i = 0; i < limite; i++) {
		if (codigoHuffman[i] == -1)	// Se o codigo de Huffman tiver comprimento 0 (valor = -1) fica '/'
			strcpy(str, "/"); 	// Esta condicao nao era necessaria, so para efeitos de debug
		else dec2bin(codigoHuffman[i], str);

		// Tratar dos zeros 'a esquerda que sao ignorados
		if ((int)strlen(str) < ccc[i]) { // O if nao precisa de ccc[i] != -1, pois strlen >= 0
			aux = ccc[i] - strlen(str);
			strcpy(straux, str);
			for (j = 0; j < aux; j++)
				str[j] = '0';
			for (j = aux; j < ccc[i]; j++)
				str[j] = straux[j-aux];
			str[ccc[i]] = '\0';
		}
#ifdef DEBUG
		if (codigoHuffman[i]!=-1)
			printf("%d:\t\t%d\t\t%d\t\t%s\n",ordem[i], ccc[i], codigoHuffman[i],str);
#endif
		if (codigoHuffman[i]!=-1)
			addNode(hft, str, ordem[i], 0);
	}
	
	delete [] str;
	delete [] straux;
}

int* get_lengths(HuffmanTree *hft, int *length, int lim, unsigned int *rb, char *availBits, FILE* gzFile) {
	int i = 0, valor=0, pos,j;

	memset(length, 0, sizeof(int)*(lim));

	while(i < lim) {

		getBits(&(*rb), 1, &(*availBits), gzFile, &valor); // Ler um bit

		if ((pos = nextNode(hft, (char)(valor+(int)'0'))) == -2); // Ainda nao ha folha

		else {
			//tratar excepcoes
			resetCurNode(hft);

			switch (pos) {

				case 16:
					getBits(&(*rb), 2, &(*availBits), gzFile, &valor);
					valor+=3;
					j=i;

					for(j=i;j<i+valor;j++)
						length[j]=length[j-1];

					i+= valor;
					break;

				case 17:
					getBits(&(*rb), 3, &(*availBits), gzFile, &valor);
					valor += 3;
					i+=valor;
					break;

				case 18:
					getBits(&(*rb), 7, &(*availBits), gzFile, &valor);
					valor += 11;
					i+=valor;

					break;
				default:
					length[i++] = pos;
				}
		}
	}
	return length;
}

unsigned char* inflate(HuffmanTree *HT_LIT, HuffmanTree *HT_DIST, unsigned int *rb, char* availBits, FILE* gzFile,unsigned char* buff, int *ind) {
	int valor, code, len=0,dist=0,i;
	unsigned int n_bits;

	while(1) {
		getBits(&(*rb), 1, &(*availBits), gzFile, &valor);
		while((code = nextNode(HT_LIT, (char)(valor+(int)'0')))==-2){
			getBits(&(*rb), 1, &(*availBits), gzFile, &valor);
		}
			resetCurNode(HT_LIT);
			if (code == 256)
				break;

			else if (code < 257){
				buff[(*ind)++]= code;

				continue;
			}	//caso entre 257 e 264 inclusive
			else{ // code >= 257

				if(code <265)
					len = code - 254;
				else if(code == 285)
					len = 258;
				else {

					n_bits = (code-265)/4 + 1;//Obter numero de bits adicionais a ler
					getBits(&(*rb), n_bits, &(*availBits), gzFile, &valor);
					//          2^(bits+2) + 3 + 2^bits ( code - (265 + 4(bits-1) ) )
					len = (1 << (n_bits+2))+3 + (1 << n_bits)*(code - (265+4*(n_bits-1))) + valor;
					//len=pow(2,n_bits+2) + 3 + pow(2,n_bits)* ( code - (265 + 4*(n_bits-1) ) )+valor;
				}
			}


		// Ler a distancia. Avan�ar na arvore enquanto n�o atingirmos uma folha */

        getBits(&(*rb), 1, &(*availBits), gzFile, &valor);
        while ((code=nextNode(HT_DIST,(char)(valor+(int)'0'))) == -2 )
					getBits(&(*rb), 1, &(*availBits), gzFile, &valor);

        resetCurNode(HT_DIST);//Fazer reset ao current node para come�ar sempre do inicio (raiz) da �rvore.

		//Obter a dist�ncia a recuar
        if(code < 4)
            dist = code + 1;

        else {
            n_bits = (code-4)/2+ 1;
			getBits(&(*rb), n_bits, &(*availBits), gzFile, &valor);

            dist = ((code-2*(n_bits+1) + 2) << n_bits) + 1 + valor;
        }

		for(i=0;i<len;i++)
			buff[(*ind)+i] = buff[(*ind)-dist+i];//Recuar a distancia pretendida e copiar os "len" simbolos seguintes para o final do buffer

		(*ind)+=len;

	}
	return buff;

}
/////////////////////////////////////////////// Fun��es auxiliares //////////////////////////////////////////////

void getBits(unsigned int *rb, int needBits, char *availBits, FILE* gzFile,int *valor) {
        unsigned char byte;
		while (*availBits < needBits)
		{
			fread(&byte, 1, 1, gzFile);
			*rb = (byte << (*availBits)) | *rb;
			(*availBits) += 8;
		}
        *valor = (*rb & needBits2Mask(needBits));
		*rb = *rb >> needBits; //descartar
		*availBits -=needBits;
}

// Funcao que converte o numero de bits para decimal
int needBits2Mask(int needBits) {
	int i, valor;
	char* aux = new char [needBits+1];

	if (aux == NULL) {
		perror("Mem error");
		exit(1);
	}
	for (i = 0; i < needBits; i++)
		aux[i] = '1';
	aux[i] = '\0';
	valor = bin2dec(aux);
	
	delete [] aux;
	
	return valor;
}

/* Ordena o array "ccc" por ordem crescente de comprimento, e, 
em caso de igual comprimento, por ordem crescente de
comprimento lexicogr�fico, baseando-se no array "ordem" */
void sortArray(int* ordem, int* ccc, int n) {
    int aux, check=0;

    for (int i=0; i<n; i++){
		check=0;
        for (int j=0;j<n-i-1;j++){
            if (ccc[j]> ccc[j+1]){
                aux=ccc[j];
                ccc[j]=ccc[j+1];
                ccc[j+1]=aux;

                aux=ordem[j];
                ordem[j]=ordem[j+1];
                ordem[j+1]=aux;

                check = 1;
            } else if (ccc[j]==ccc[j+1] &&  ordem[j]>ordem[j+1]){
                aux=ordem[j];
                ordem[j]=ordem[j+1];
                ordem[j+1]=aux;
                check = 1;
            }
        }
        if (check==0)
            break;
    }

}

////////////////////////////////////////////////// Conversores //////////////////////////////////////////////////

int bin2dec(char *bin) {
	int b, k, m, n, len, sum = 0;

	len = strlen(bin) - 1;
	for (k = 0; k <= len; k++) {
		n = bin[k] - 48;
		for (b = 1, m = len; m > k; m--)
			b *= 2;
		sum += n * b;
	}
	return sum;
}

void dec2bin(int decimal, char *binary) {
	int  k = 0, n = 0, resto;
	char temp[80];

	do {
		resto = decimal % 2;
		decimal /= 2;
		temp[k++] = resto + 48;
	} while (decimal > 0);

	while (k >= 0)
		binary[n++] = temp[--k];

	binary[n-1] = '\0';
}

//---------------------------------------------------------------
//L� o cabe�alho do ficheiro gzip: devolve erro (-1) se o formato for inv�lidodevolve, ou 0 se ok
int getHeader(FILE *gzFile, gzipHeader *gzh) //obt�m cabe�alho
{
	unsigned char byte;

	//Identica��o 1 e 2: valores fixos
	fread(&byte, 1, 1, gzFile);
	(*gzh).ID1 = byte;
	if ((*gzh).ID1 != 0x1f) return -1; //erro no cabe�alho

	fread(&byte, 1, 1, gzFile);
	(*gzh).ID2 = byte;
	if ((*gzh).ID2 != 0x8b) return -1; //erro no cabe�alho

	//M�todo de compress�o (deve ser 8 para denotar o deflate)
	fread(&byte, 1, 1, gzFile);
	(*gzh).CM = byte;
	if ((*gzh).CM != 0x08) return -1; //erro no cabe�alho

	//Flags
	fread(&byte, 1, 1, gzFile);
	unsigned char FLG = byte;

	//MTIME
	char lenMTIME = 4;
	fread(&byte, 1, 1, gzFile);
	(*gzh).MTIME = byte;
	for (int i = 1; i <= lenMTIME - 1; i++)
	{
		fread(&byte, 1, 1, gzFile);
		(*gzh).MTIME = (byte << 8) + (*gzh).MTIME;
	}

	//XFL (not processed...)
	fread(&byte, 1, 1, gzFile);
	(*gzh).XFL = byte;

	//OS (not processed...)
	fread(&byte, 1, 1, gzFile);
	(*gzh).OS = byte;

	//--- Check Flags
	(*gzh).FLG_FTEXT = (char)(FLG & 0x01);
	(*gzh).FLG_FHCRC = (char)((FLG & 0x02) >> 1);
	(*gzh).FLG_FEXTRA = (char)((FLG & 0x04) >> 2);
	(*gzh).FLG_FNAME = (char)((FLG & 0x08) >> 3);
	(*gzh).FLG_FCOMMENT = (char)((FLG & 0x10) >> 4);

	//FLG_EXTRA
	if ((*gzh).FLG_FEXTRA == 1)
	{
		//ler 2 bytes XLEN + XLEN bytes de extra field
		//1� byte: LSB, 2�: MSB
		//char lenXLEN = 2; ------------------>  warning removido

		fread(&byte, 1, 1, gzFile);
		(*gzh).xlen = byte;
		fread(&byte, 1, 1, gzFile);
		(*gzh).xlen = (byte << 8) + (*gzh).xlen;

		(*gzh).extraField = new unsigned char[(*gzh).xlen];

		//ler extra field (deixado como est�, i.e., n�o processado...)
		for (int i = 0; i <= (*gzh).xlen - 1; i++)
		{
			fread(&byte, 1, 1, gzFile);
			(*gzh).extraField[i] = byte;
		}
	}
	else
	{
		(*gzh).xlen = 0;
		(*gzh).extraField = 0;
	}

	//FLG_FNAME: ler nome original
	if ((*gzh).FLG_FNAME == 1)
	{
		(*gzh).fName = new char[1024];
		unsigned int i = 0;
		do
		{
			fread(&byte, 1, 1, gzFile);
			if (i <= 1023)  //guarda no m�ximo 1024 caracteres no array
				(*gzh).fName[i] = byte;
			i++;
		}while(byte != 0);
		if (i > 1023)
			(*gzh).fName[1023] = 0;  //apesar de nome incompleto, garantir que o array termina em 0
	}
	else
		(*gzh).fName = 0;

	//FLG_FCOMMENT: ler coment�rio
	if ((*gzh).FLG_FCOMMENT == 1)
	{
		(*gzh).fComment = new char[1024];
		unsigned int i = 0;
		do
		{
			fread(&byte, 1, 1, gzFile);
			if (i <= 1023)  //guarda no m�ximo 1024 caracteres no array
				(*gzh).fComment[i] = byte;
			i++;
		}while(byte != 0);
		if (i > 1023)
			(*gzh).fComment[1023] = 0;  //apesar de coment�rio incompleto, garantir que o array termina em 0
	}
	else
		(*gzh).fComment = 0;


	//FLG_FHCRC (not processed...)
	if ((*gzh).FLG_FHCRC == 1)
	{
		(*gzh).HCRC = new unsigned char[2];
		fread(&byte, 1, 1, gzFile);
		(*gzh).HCRC[0] = byte;
		fread(&byte, 1, 1, gzFile);
		(*gzh).HCRC[1] = byte;
	}
	else
		(*gzh).HCRC = 0;

	return 0;
}


//---------------------------------------------------------------
//Analisa block header e v� se � huffman din�mico
int isDynamicHuffman(unsigned char rb)
{
	unsigned char BTYPE = rb & 0x03;

	if (BTYPE == 0) //--> sem compress�o
	{
		printf("Ignorando bloco: sem compacta��o!!!\n");
		return 0;
	}
	else if (BTYPE == 1)
	{
		printf("Ignorando bloco: compactado com Huffman fixo!!!\n");
		return 0;
	}
	else if (BTYPE == 3)
	{
		printf("Ignorando bloco: BTYPE = reservado!!!\n");
		return 0;
	}
	else
		return 1;
}


//---------------------------------------------------------------
//Obt�m tamanho do ficheiro original
long getOrigFileSize(FILE * gzFile)
{
	//salvaguarda posi��o actual do ficheiro
	long fp = ftell(gzFile);

	//�ltimos 4 bytes = ISIZE;
	fseek(gzFile, -4, SEEK_END);

	//determina ISIZE (s� correcto se cabe em 32 bits)
	unsigned long sz = 0;
	unsigned char byte;
	fread(&byte, 1, 1, gzFile);
	sz = byte;
	for (int i = 0; i <= 2; i++)
	{
		fread(&byte, 1, 1, gzFile);
		sz = (byte << 8*(i+1)) + sz;
	}


	//restaura file pointer
	fseek(gzFile, fp, SEEK_SET);

	return sz;
}


//---------------------------------------------------------------
void bits2String(char *strBits, unsigned char byte)
{
	char mask = 0x01;  //get LSbit
	strBits[8] = 0;
	for (char bit, i = 7; i >= 0; i--)
	{
		bit = byte & mask;
		strBits[(int)i] = bit + 48; //converter valor num�rico para o caracter alfanum�rico correspondente
		byte = byte >> 1;
	}
}
